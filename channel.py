# -*- coding: utf-8 -*-

import re
from datetime import datetime
from gpiozero import LED

class OutChannel:
    ''' třída pro nastavení parametrů výstupních kanálů
        
        id:    1-8 triaky; 9-10 relé
        addr:  je dána zapojením na expand DPS
               číslování dle GPIO (nikoli číslem pinu na 2×20 konektoru)  
        type:  typ výstupního kanálu:  
               - heat      (udržuje teplotu pro daný čas)  
               - fountain  (spouští fontánku při nízké vlhkosti - ON|OFF)  
               - fogger    (spouší rozprašovač délka pulsu (ms) + mezera mezi pulsy(s))
               - light     (rozsvěcí světlo v daném čase)  
        name:  pojmenování výstupního kanálu  
        state: bool (ON | OFF) - sepnuto/nesepnuto  
        used:  bool kanál je/není využíván  
        sens:  vstupní senzor ovládající kanál (ID)
        bypass: konec bypass režimu, v minutách od počátku epochy (-1 = trvale)
        pulse: délka pulsu spuštěné mlhy (sec)
        delay: délka prodlevy mezi pulsy mlhovače (min)
        fogger_trig: čas spuštění v min od epochy
    '''
    
    
    def  __init__(self, id=None, type=None, name="", used=None, sens=None, pulse=0, delay=0):
        self.SetID(id)
        self.type = type
        self.name = name
        self.state = None
        self.used = used
        self.pulse = pulse 
        self.delay = delay
        self.sens = sens
        self.bypass = 0
        self.fogger_trig = None
    
    # nastavení ID kanálu a jemu přiřazenou adresu GPIO dle zapojení
    # expanzní desky pro Raspberry Zero
    def SetID(self,i=None):
        ''' podle id nastavuje adresu GPIO portu
        
            RELE1 = LED(23)
            RELE2 = LED(24)

            triak = [
                LED(12),    # SSR 01
                LED(13),    # SSR 02
                LED(19),    # SSR 03
                LED(16),    # SSR 04
                LED(26),    # SSR 05
                LED(20),    # SSR 06
                LED(21),    # SSR 07
                LED(6)      # SSR 08
            ]
        '''
        
        # chybné volání bez adresy
        if i==None:
            return 1
        
        # mapa GPIO portů
        if i==0:
            self.id = 0
            self.addr = 12
            
        elif i==1:
            self.id = 1
            self.addr = 13
            
        elif i==2:
            self.id = 2
            self.addr = 19

        elif i==3:
            self.id = 3
            self.addr = 16

        elif i==4:
            self.id = 4
            self.addr = 26

        elif i==5:
            self.id = 5
            self.addr = 20

        elif i==6:
            self.id = 6
            self.addr = 21

        elif i==7:
            self.id = 7
            self.addr = 6

        elif i==8:
            self.id = 8
            self.addr = 23

        elif i==9:
            self.id = 9
            self.addr = 24

        else:
            self.id = None
            self.addr = None
            return 2
        
        return 0
    
    def viewUsed(self):
        if self.used:
            return "ON"
        else:
            return "OFF"
        
    def viewState(self):
        if self.state == True:
            return "ON"
        else:
            return "OFF"
            
    def viewBypass(self):
        if self.bypass == 0:
            return ""
        elif self.bypass == -1:
            return "manual"
        else:
            try:
                # // je celočíselné dělení
                # %  je zbytek po dělění

                if (self.bypass == 0) or (self.bypass == None):
                    return "00:00"

                x = self.bypass - int(datetime.timestamp(datetime.now()))//60

                return "{:0>2}:{:0>2}".format(x//60,x%60)
            
            except:
                return "err"

    def viewTemp(self,t=0):
        if self.sens == None:
            return ""
        
        if t[self.sens] == None:
            return "err"
        
        if t[self.sens] <= 10:
            return "err"
        else:
            return "{:.1f}°C".format(t[self.sens])

    def viewHumi(self,h):
        if self.sens == None:
            return ""
        
        if h[self.sens] == None:
            return "err"
        
        if h[self.sens] <= 10:
            return "err"
        else:
            return "{:.1f} %".format(h[self.sens])

    def viewType(self):
        if self.type == "heat":
            return "topení"
        elif self.type == "fogger":
            return "mlha"
        elif self.type == "fountain":
            return "fontánka"
        elif self.type == "light":
            return "osvětlení"
        else:
            return "err"

    def viewSens(self):
        if self.sens == None:
            return "None"
        else:
            return self.sens + 1
        
    def SetBypass(self,bps_time=""):
        
        # ořezání vstupu od případného bílého balastu
        buff = bps_time.strip()

        # prázdný řetězec - předpoklad požadavku konce bypassu
        if buff == "":
            self.bypass = 0
            return 0
        
        if buff == "0":
            self.bypass = 0
            return 0
        
        # -1 je pro manuální režim
        if buff == "-1":
            self.bypass = -1
            return 0
        
        # aktuální čas od epochy v minutách
        epo = int(datetime.timestamp(datetime.now()))//60    
            
        # očekávám zadání buď v celých hodinách,
        # nebo v hodinách a minutách, nebo části hodinách
        # 5   0:10    2:30    1,5  2.8
        # náhrada čárky za tečku -> převod na float a vynásobení 10 -> převod na int   
        buff = buff.replace(',','.')
             
        try:
            r = re.match(r"\d{1,3}",buff)
            if r[0] == buff:
                x = int(buff)*60

                if 0 < x:
                    self.bypass = x + epo
                    return 0
                else:
                    print("Chyba: požadovaný čas bypassu je mimo povolený rozsah")
                    return 3
            
        except TypeError:
            print("Chyba: špatné zadání časového rámce")
            return 1
        
        if '.' in buff:
            h,m = buff.split('.')
            
            try:
                hod = int(h)
                # použiji pouze první cifru za desetinnou tečkou
                min = int(m[0]) * 6
                
                self.bypass = hod * 60 + min + epo
                return 0
                
            except ValueError:
                print("Chyba: zadání času není číselné")
                return 1
        
        if ':' in buff:
            h,m = buff.split(':')
            
            try:
                hod = int(h)
                min = int(m)
                if min > 59:
                    print("Chyba: chybné zadání časového údaje")
                    return 2
                self.bypass = hod * 60 + min + epo
                return 0
                
            except ValueError:
                print("Chyba: zadání času není číselné")
                return 1
        
        # žádný očekávaný tvar zadání
        return 4
    
