# -*- coding: utf-8 -*-

# importy knihoven
from flask import Flask, render_template, request
from time import sleep
from datetime import datetime
import ntplib
from os import system

import subprocess
import threading
import re
import sqlite3

# RPi importy
from gpiozero import LED
from gpiozero import Button
import board
import adafruit_dht

# import vlasní třídy pro uložení dat z terminálů
from terrarium import Terra
terra = Terra()

# import vlastní třídy pro ovládání alertu do emailu
from alert import Alert
emailAlert = Alert()


triak = [
    LED(12),    # SSR 01
    LED(13),    # SSR 02
    LED(19),    # SSR 03
    LED(16),    # SSR 04
    LED(26),    # SSR 05
    LED(20),    # SSR 06
    LED(21),    # SSR 07
    LED(6),     # SSR 08
    LED(23),    # Relé 1
    LED(24)     # Relé 2
]

# test triaků
# while 1:
    # for i in range(8):
    #     triak[i].on()
    #     sleep(1)
    #     triak[i].off()
        
# nastavení senzorů
# pozor board.D4 není přístupný při spoštění skriptu jako root!!!
sens1 = adafruit_dht.DHT22(board.D4)
sens2 = adafruit_dht.DHT22(board.D17)

# # test sensorů
# while 1:
    # system('clear')
    
    # print("Test senzoru 1")
    
    # try:
    #     # Print the values to the serial port
    #     temperature_c = sens1.temperature
    #     temperature_f = temperature_c * (9 / 5) + 32
    #     humidity = sens1.humidity
    #     print("Temp: {:.1f} F / {:.1f} C    Humidity: {}% \n"
    #           .format(temperature_f, temperature_c, humidity))

    # except RuntimeError as error:
    #     # Errors happen fairly often, DHT's are hard to read, just keep going
    #     print(error.args[0])
    #     print('\n')

    # print("Test senzoru 2")
    # try:
    #     # Print the values to the serial port
    #     temperature_c = sens2.temperature
    #     temperature_f = temperature_c * (9 / 5) + 32
    #     humidity = sens2.humidity
    #     print("Temp: {:.1f} F / {:.1f} C    Humidity: {}% \n"
    #           .format(temperature_f, temperature_c, humidity))

    # except RuntimeError as error:
    #     # Errors happen fairly often, DHT's are hard to read, just keep going
    #     print(error.args[0])
    #     print('\n')
        
    # sleep(3.0)


NTP_hour = 0   # hodina, kdy byla naposled aktualizována časová značka z NTP serveru
NTP_sever1 = 'tik.cesnet.cz'
NTP_sever2 = 'tak.cesnet.cz'
NTP_sever3 = 'pool.ntp.org'
NTP_flag   = False

buff_temp = [0,0]
buff_humi = [0,0]

Prusa3D_flag = False
Prusa3D_TopBox  = 0.0
Prusa3D_BottBox = 0.0
Prusa3D_Hyst    = 2.5
Prusa3D_Timer   = 60      # interval záznamu v sec
buff_min = -1

    
def terra_thread():
    
    # parametr pro opakované pokusy čtení senzorů 
    # (počet ignorování neplatných pokusů, 
    # kdy bude považována za platnou předchozí vyčtená hodnota)
    BUFF_ERR_TRESHOLD = 10
    buff_err0 = 0
    buff_err1 = 0
    
    global buff_temp
    global buff_humi

    
    # nekonečná smyčka...
    while 1:
        global Prusa3D_flag
             
        # nejprve vyčtení aktuálních dat na senzorech
        try:
            buff_temp[0] = sens1.temperature
            buff_humi[0] = sens1.humidity
            buff_err0 = 0
            # try:
            #     print("teplota: {:.1f}°C   Vlhkost: {}% ".format(buff_temp1,buff_humi1))
            # except:
            #     pass
                        
        except RuntimeError as error:
            # Errors happen fairly often, DHT's are hard to read, just keep going
            
            if buff_err0 < BUFF_ERR_TRESHOLD:
                buff_err0 += 1
            else:
                print(error.args[0])
                buff_err1 = 0
                buff_temp[0] = None
                buff_humi[0] = None
            
        try:
            buff_temp[1] = sens2.temperature
            buff_humi[1] = sens2.humidity
            buff_err1 = 0
            # try:
            #     print("teplota: {:.1f}°C   Vlhkost: {}% ".format(buff_temp2,buff_humi2))
            # except:
            #     pass
                        
        except RuntimeError as error:
            # Errors happen fairly often, DHT's are hard to read, just keep going
            
            if buff_err1 < BUFF_ERR_TRESHOLD:
                buff_err1 += 1
            else:
                print(error.args[0])
                buff_err1 = 0
                buff_temp[1] = None
                buff_humi[1] = None
        
        # režim Průša 3D Box
        if Prusa3D_flag:
            
            
            if buff_temp[0] == None or buff_temp[1] == None:
                print ("Sensor Error…")
                continue
            
            
            # horní skříň
            if (Prusa3D_TopBox - Prusa3D_Hyst) > buff_temp[0]:
                triak[0].off()  # červená
                triak[1].off()  # zelená
                triak[2].on()   # modrá
            elif (Prusa3D_TopBox + Prusa3D_Hyst) < buff_temp[0]:
                triak[0].on()   # červená
                triak[1].off()  # zelená
                triak[2].off()  # modrá
            else:
                triak[0].off()  # červená
                triak[1].on()   # zelená
                triak[2].off()  # modrá
            
            # spodní skříň
            if (Prusa3D_BottBox - Prusa3D_Hyst) > buff_temp[1]:
                triak[5].off()  # červená
                triak[6].off()  # zelená
                triak[7].on()   # modrá
            elif (Prusa3D_BottBox + Prusa3D_Hyst) < buff_temp[1]:
                triak[5].on()   # červená
                triak[6].off()  # zelená
                triak[7].off()  # modrá
            else:
                triak[5].off()  # červená
                triak[6].on()   # zelená
                triak[7].off()  # modrá
            
            # sběr dat do databáze
            # časové razítko v sec
            global buff_min
            now = int(datetime.now().timestamp())
            
            # spuštění záznamu v daných intervalech
            if buff_min == -1 or (buff_min + Prusa3D_Timer) < now:
                
                # otevírám databázi
                with sqlite3.connect("data/prusa.sqlite3") as db:
                    cur = db.cursor()
                    cur.execute('''INSERT INTO prusa(
                        timestamp, 
                        top_temp, 
                        top_humi,
                        bott_temp,
                        bott_humi)
                    VALUES(?,?,?,?,?)''', (
                        now,
                        buff_temp[0],
                        buff_humi[0],
                        buff_temp[1],
                        buff_humi[1]))
                    db.commit()
                
                
                buff_min = now
                
            
            
            
            # zbývající část kódu přeskakuji
            continue
          
        # ...kde procházím každý jednotlivý kanál a kontroluji jeho aktuální parametry
        for chann in terra.channel:
            
            # a vypnuté kanály přeskakuji, že...
            if chann.used == False:
                continue
            
            # trvale sepnuto (manuální režim)
            if chann.bypass == -1:
                chann.state = True
                continue
            
            # bypass režim
            if chann.bypass > 0:
                x = chann.bypass - int(datetime.timestamp(datetime.now()))//60
                
                if x <= 0:
                    chann.bypass = 0
                    chann.state = False
                    continue
                
                # bypas JE stále aktivní, kanál musí být sepnut
                chann.state = True
                continue
            
            # kontrola zadaných programů/intervalů
                        
            for i in terra.interval:
                # zjistit aktuální den
                # datetime: 0=po, ..., 6=ne
                # timetuple() je struktura s rozkladem data a času na číselné hodnoty
                # potřebuji:
                #      .tm_wday (den v týdnu, počítáno od nuly, 0=po, 6=ne)
                #      .tm_min
                #      .tm_hour
                akt_cas = datetime.timetuple(datetime.now())
                
                akt_den = akt_cas.tm_wday + 1
                akt_min = akt_cas.tm_hour * 60 + akt_cas.tm_min
                
                # nejprve test kanálu…
                if  chann.id in i.ch_outs:
                    
                    # …pak den v týdnu…
                    if akt_den in i.days:
                        
                        # …a nakonec časový interval…
                        if akt_min >= i.od and akt_min <= i.do:
                            
                            # program je aktuální, zjišťuji typ a parametry kanálu
                            
                            # výstupní kanál ovládá topení
                            if chann.type == "heat":
                                
                                # nutno zahrnout hysterezi
                                # kanál je sepnut, topení topí, teplota jde vzhůru
                                try:
                                    if chann.state == True:
                                        if int(buff_temp[chann.sens] *10) < i.temp:
                                            chann.state = True
                                            continue
                                        else:
                                            chann.state = False
                                            continue
                                except:
                                    print("Chyba čidla na kanále {}".format(chann.sens))
                                    
                                    
                                                                   
                                # kanál je vypnut, topení netopí, teplota jde dolů
                                # započítávám hysterezi
                                else:
                                    try:
                                        if int(buff_temp[chann.sens] *10) < (i.temp - terra.HYST_TEMP):
                                            chann.state = True
                                            continue
                                        else:
                                            chann.state = False
                                            continue
                                    except:
                                        print("Chyba čidla na kanále {}".format(chann.sens))
                                    
                              
                            # výstupní kanál ovládá vlhkost (přes fontánku, či rozprašovač)
                            elif chann.type == "fountain" or chann.type == "fogger":
                            
                                # nutno zahrnout hysterezi
                                # kanál je sepnut, topení topí, teplota jde vzhůru
                                if chann.state == True:
                                    try:
                                        if int(buff_humi[chann.sens] *10) < i.humi:
                                            chann.state = True
                                            continue
                                        else:
                                            chann.state = False
                                            continue
                                    except:
                                        print("Chyba čidla na kanále {}".format(chann.sens))
                               
                                # kanál je vypnut, topení netopí, teplota jde dolů
                                # započítávám hysterezi
                                else:
                                    try:
                                        if int(buff_humi[chann.sens] *10) < (i.humi - terra.HYST_HUMI):
                                            chann.state = True
                                            continue
                                        else:
                                            chann.state = False
                                            continue
                                    except:
                                        print("Chyba čidla na kanále {}".format(chann.sens))      
                                                             
                            
                            # výstupní kanál ovládá osvětlení
                            elif chann.type == "light":
                                
                                if i.light == True:
                                    chann.state = True
                                    
                                else:
                                    chann.state = False
                    
                    else:
                        chann.state = False
                
            
       # promítnutí status výstupních kanálů do HW RPi
        for i in range(10):
            if terra.channel[i].state == True:
                
                # zvláštní péče o mlhovač...
                if terra.channel[i].type == 'fogger':
                    
                    # aktuální čas od epochy v minutách
                    epo = int(datetime.timestamp(datetime.now()))//60
                    
                    # je už aktivní?
                    if terra.channel[i].fogger_trig == None:
                        # není, tak poprvé zapneme
                        triak[i].on()
                        
                        # počkám definovanou chvilku
                        sleep(terra.channel[i].pulse)
                        
                        # a vypnu
                        triak[i].off()
                        
                        # a nastavím další spuštění
                        terra.channel[i].fogger_trig = epo + terra.channel[i].delay
                        
                    # dosáhli jsme času spuštění    
                    elif terra.channel[i].fogger_trig <= epo:
                        
                        # ponejprve posuneme spuštění zase o kousek dál
                        terra.channel[i].fogger_trig += terra.channel[i].delay
                        
                        triak[i].on()
                        sleep(terra.channel[i].pulse)
                        print("mlžím...")
                        print("epo: {} min.".format(epo))
                        triak[i].off()
                    
                    # ještě není čas na mlžení
                    else:
                        pass
                    
                    
                else:
                    triak[i].on()
                    
            else:
                # zvláštní péče o mlhovač...
                if terra.channel[i].type == 'fogger':
                    
                    # vypneme spuštěč
                    terra.channel[i].fogger_trig = None
                
                triak[i].off()
            
            


# zapouzdření instance třídy Flask do funkce 
# pro spuštění ve vlákně na pozadí
def threadWeb():
    WebServer = Flask(__name__)
   
    # stránka indexu
    @WebServer.route("/", methods = ["GET", "POST"])
    def index():
        
        global buff_temp
        global buff_humi
        global Prusa3D_flag
        global Prusa3D_BottBox
        global Prusa3D_TopBox
        global Prusa3D_Hyst
        
        bps_form = request.form
        
        # test příjmu dat z formuláře
        if len(bps_form):
            
            # data ze stránky bypass
            if bps_form["IDform"] == "bypass":
                
                i = int(bps_form['address'])
                terra.channel[i].SetBypass(bps_form['bps_time'])
            
            if bps_form["IDform"] == "alert":
                
                try:
                    emailAlert.sens1TempTop = int(float(bps_form['1tempTop']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                try:
                    emailAlert.sens1TempBot = int(float(bps_form['1tempBot']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                try:
                    emailAlert.sens1HumiTop = int(float(bps_form['1humiTop']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                try:
                    emailAlert.sens1HumiBot = int(float(bps_form['1humiBot']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                try:
                    emailAlert.sens2TempTop = int(float(bps_form['2tempTop']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                try:
                    emailAlert.sens2TempBot = int(float(bps_form['2tempBot']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                try:
                    emailAlert.sens2HumiTop = int(float(bps_form['2humiTop']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                try:
                    emailAlert.sens2HumiBot = int(float(bps_form['2humiBot']) *10)
                except:
                    print("chybná vstupní data z formuláře alert")

                emailAlert.SaveSetup()
                
                emailAlert.LoadSetup()
            
        return render_template("index.html", chann=terra.channel, t=buff_temp, h=buff_humi, v=terra.VERSION, prusa=Prusa3D_flag, tt=Prusa3D_TopBox, bt=Prusa3D_BottBox, hyst=Prusa3D_Hyst)

    # stránky s nastavením bypassu pro jednotlivé kanály
    #   - v index.html jsou generovány odkazy podle ID kanálu
    #   - do šablony posílám jen příslušnou položku seznamu podle dohledaného indexu
    @WebServer.route("/bypass/<int:chID>/", methods = ["GET", "POST"])
    def bypass(chID):
        
        global Prusa3D_flag
        
        return render_template("bypass.html",chann=terra.channel[chID],prusa=Prusa3D_flag)


   
    # stránka s přehledem parametrů jednotlivých kanálů
    #--------------------------------------------------
    @WebServer.route("/view_nast", methods = ["GET", "POST"])
    def view_nast():
        
        s_form = request.form
        
        # test příjmu dat
        if len(s_form):
            
            # zjistím id kanálu
            f_id = int(s_form['id'])
            
            # a přepíšu vložená data do parametrů kanálu
            terra.channel[f_id].name = s_form['pojmenovani']
            terra.channel[f_id].type = s_form['type']
            if s_form['used'] == 'on':
                terra.channel[f_id].used = True
            else:
                terra.channel[f_id].used = False
            
            if s_form['sens'] == 'None':
                terra.channel[f_id].sens = None
            else:
                terra.channel[f_id].sens = int(s_form['sens']) - 1
                
            try:
                terra.channel[f_id].pulse = int(s_form['pulse'])
            except:
                print("chybně zadané číslo pro pulse")
                terra.channel[f_id].pulse = 0
                
            try:
                terra.channel[f_id].delay = int(s_form['delay'])
            except:
                print("chybně zadané číslo pro delay")
                terra.channel[f_id].delay = 0
            
            # a až to budu umět, tak uložím do konfiguračního souboru
            terra.UlozitKonfiguraci()
            
            
        return render_template("view_nast.html", chann=terra.channel)
    
    
    # stránka pro reboot
    @WebServer.route("/rpi", methods = ["GET", "POST"])
    def rpi():
        
        global Prusa3D_flag
        global Prusa3D_BottBox
        global Prusa3D_TopBox
        global Prusa3D_Hyst
        
        bps_form = request.form
        
        # test příjmu dat z formuláře
        if len(bps_form):
            try:
                if bps_form['RPi'] == 'True':
                
                    print("Restartuji zařízení, papa...")
                    subprocess.run(["sudo", "shutdown", "-r", "now"])
            except:
                pass
            
            try:
                if bps_form['Prusa3D'] == 'True':
                    print('Nastavuji režim Průša 3D Printer Box na zapnutý…')
                    
                    Prusa3D_flag = True
                    
                    try:
                        Prusa3D_TopBox  = float(bps_form['TopBox'])
                        Prusa3D_BottBox = float(bps_form['BottBox'])
                        Prusa3D_Hyst    = float(bps_form['Hystereze'])
                    
                        # zápis parametru do ini souboru
                        path = "config/application.ini"
                        
                        with open(path,mode="w",encoding="utf-8") as f:
                            f.write("Prusa3D: On\n")
                            f.write("TopBoxTemp: {}\n".format(Prusa3D_TopBox))
                            f.write("BottBoxTemp: {}\n".format(Prusa3D_BottBox))
                            f.write("Hystereze: {}\n".format(Prusa3D_Hyst))
                    except:
                        pass
            except:
                pass
            
            try:
                if bps_form['Prusa3D'] == 'False':
                    print('Nastavuji režim Průša 3D Printer Box na vypnutý…')
                    
                    Prusa3D_flag = False
                    
                    # zápis parametru do ini souboru
                    path = "config/application.ini"
                    
                    with open(path,mode="w",encoding="utf-8") as f:
                        f.write("Prusa3D: Off\n")
                        f.write("TopBoxTemp: {}\n".format(Prusa3D_TopBox))
                        f.write("BottBoxTemp: {}\n".format(Prusa3D_BottBox))
                        f.write("Hystereze: {}\n".format(Prusa3D_Hyst))
            except:
                pass
                
            
        return render_template("rpi.html", chann=terra.channel, prusa=Prusa3D_flag, tt=Prusa3D_TopBox, bt=Prusa3D_BottBox, hyst=Prusa3D_Hyst)
    
    
    # stránka pro upgrade
    @WebServer.route("/upg", methods = ["GET", "POST"])
    def upg():
        
        bps_form = request.form
        
        # test příjmu dat z formuláře
        if len(bps_form):
            
            if bps_form['RPi'] == 'True':
                print("Stahuji nové kódy z GitLabu...")
                subprocess.run(['sh', '/home/pi/terrarium/git_pull.sh'])
            
            
        return render_template("upg.html")
    
    
    # stránky s nastavením konkrétního kanálu
    # data jsou odesílána a zpracována u stránky s přehledem!
    #--------------------------------------------------------
    @WebServer.route("/chann/<int:chann_id>")
    def nastaveni(chann_id):
        
        return render_template("nastaveni.html",chann=terra.channel[chann_id])
    
    
    # stránka s nastavením intervalů
    # data jsou odesílána a zpracována u stránky s přehledem!
    #--------------------------------------------------------
    @WebServer.route("/view_int", methods = ["GET", "POST"])
    def view_int():
        
        s_form = request.form
                
        # flag 0=nic nezobrazit
        #      1=zobrazit tlačítko k uložení změn
        #      2=zobrazit hlášení o korektním uložení
        flag = 0
        
        # test příjmu dat z formuláře
        if len(s_form):
            
            # test na příkaz k uložení záznamů
            try:
                # pokyn k uložení změněných záznamů
                if s_form['save'] != None:
                    flag = 2
                    
                    print("Pokus o uložení intervalů...")
                    
                    err = terra.UlozitIntervaly()
                
                    return render_template("view_int.html",interval=terra.interval,channels=terra.channel,fl_save=flag)
            except:
                pass
            
            # test na smazání záznamu
            try:
                if s_form['ID_del'] != None:
                    err = 0
            except:
                err = 1
                
            if err == 0:        
                flag = 1
                
                index = int(s_form['ID_del'])
                
                # odstranění položky ze seznamu
                terra.interval.pop(index)
                
                # nutno přeindexovat celý seznam
                index=0
                
                for b in terra.interval:
                    b.id = index
                    index +=1
                
                return render_template("view_int.html",interval=terra.interval,channels=terra.channel,fl_save=flag)
                
            
            flag = 1
            
            # vyčtení ID intervalu
            ID = int(s_form['ID'])
            
            
            #vyčtení seznamu checkboxíků se seznamem výstupních kanálů
            r = s_form.getlist('channs')
            
            # vyčtení seznamu checkboxíků se zvolenými dny
            d = s_form.getlist('days')
            
            # debug výpis          
            print("{} místnosti\n".format(len(r)))
            print(r)    
            print("{} dny\n".format(len(d)))
            print(d)
            
            terra.interval[ID].SetChannelOuts(r)
            terra.interval[ID].SetDays(d)
            
            # počátek intervalu (_OD_)
            o = s_form['od']
            terra.interval[ID].SetOd(o)
            
            # konec intervalu (_DO_)
            d = s_form['do']
            terra.interval[ID].SetDo(d)
            
            # požadovaná teplota
            t = s_form['temp']
            terra.interval[ID].SetTemp(t)
            
            # požadovaná vlhkost
            h = s_form['humi']
            terra.interval[ID].SetHumi(h)
            
            # požadované osvětlení (on|off)
            l = s_form['light']
            terra.interval[ID].SetLight(l)
        
        return render_template("view_int.html",interval=terra.interval,channels=terra.channel,fl_save=flag)
    
    
    @WebServer.route("/alert", methods = ["GET", "POST"])
    def alert():
        
        return render_template("alert.html", alert=emailAlert)    
    
    
    @WebServer.route("/interval/<int:int_id>")
    def interval(int_id):
        
        index = int_id
        
        # ID 1000 je pro nový interval
        if index == 1000:
            index = terra.AddNewInt()
            
        return render_template("intervaly.html",id=index, interval=terra.interval[index], channels=terra.channel)

    @WebServer.route("/delint/<int:int_id>")
    def delint(int_id):
        
        index = int_id
        return render_template("delete.html",id=index, interval=terra.interval[index], channels=terra.channel)

    

    # spuštění web serveru
    WebServer.run(debug=True, use_reloader=False, host="0.0.0.0", port="5000")
    

# ---------------------------------
# spuštění html serveru
# ---------------------------------

# spuštění instance třídy Flask / aplikace
if __name__ == "__main__":
    
    # načítám parametry aplikace
    path = "config/application.ini"
    
    with open(path,mode="r",encoding="utf-8") as f:
        for line in f:
            # rozdělím řádek na klíč a hodnotu, oddělovačem je dvojtečka s mezerou
            try:
                key,val = line.split(': ')
            # ignoruji nesmysly
            except:
                key = ""
                val = ""
            
            if key == 'Prusa3D':
                
                if val == 'Off':
                    Prusa3D_flag = False
                else:
                    Prusa3D_flag = True
                    
            if key == 'TopBoxTemp':
                try:
                    Prusa3D_TopBox = float(val)
                except:
                    Prusa3D_TopBox = -1.0
                    
            if key == 'BottBoxTemp':
                try:
                    Prusa3D_BottBox = float(val)
                except:
                    Prusa3D_BottBox = -1.0
                    
            if key == 'Hystereze':
                try:
                    Prusa3D_Hyst = float(val)
                except:
                    Prusa3D_Hyst = 2.5

    
    # threadWeb()
    t_WebApp = threading.Thread(name='Flask Terrarium', target=threadWeb)
    t_WebApp.setDaemon(True)
    t_WebApp.start()
    
    try:
        terra_thread()

    except KeyboardInterrupt:
        print("končím...")
        exit(0)
    
    
    