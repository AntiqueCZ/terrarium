# -*- coding: utf-8 -*-
import smtplib, ssl
from email.message import EmailMessage
import re

class Alert:
    '''třída pro ovládání vyvolání poplachu
    
    
    '''
    def __init__(self):
        self.state    = False
        self.smtp     = ""
        self.port     = 0
        self.addrFrom = ""
        self.passw    = ""
        self.addrTo   = ""
        self.predmet  = ""
        self.zprava   = ""
        
        self.flagTime1 = 0
        self.flagTime2 = 0
        
        # hodnoty jsou v 1/10°C resp. %
        self.sens1TempTop = 0
        self.sens1TempBot = 0
        self.sens1HumiTop = 0
        self.sens1HumiBot = 0
        self.sens2TempTop = 0
        self.sens2TempBot = 0
        self.sens2HumiTop = 0
        self.sens2HumiBot = 0
        
        self.LoadSetup()
        
    def LoadSetup(self):
        '''načtení parametrů ze souboru
        '''
        
        path = "config/alert.ini"
        patt = r'[^@]+@[^@]+\.[^@]+'
        
        with open(path,mode="r",encoding="utf-8") as f:
            
            for line in f:
                
                # rozdělím řádek na klíč a hodnotu, oddělovačem je dvojtečka s mezerou
                try:
                    key,val = line.split(': ')
                    val = val.strip()
                    
                # ignoruji nesmysly
                except:
                    continue
                    
                if key == "status":
                    if val == "On":
                        self.state = True
                    else:
                        self.state = False
                        
                elif key == "addrTo":
                    # kontrola elementární správnosti emailu
                    if re.fullmatch(patt,val):
                        self.addrTo = val
                    else:
                        # adresa je špatná, vypínám alert
                        self.state = False
                        self.addrTo = ""
                        
                elif key == "addrFrom":
                    # kontrola elementární správnosti emailu
                    if re.fullmatch(patt,val):
                        self.addrFrom = val
                    else:
                        # adresa je špatná, vypínám alert
                        self.state = False
                        self.addrFrom = ""
                        
                elif key == "smtp":
                    self.smtp = val
                    
                elif key == "port":
                    try:
                        self.port = int(val)
                    except:
                        # špatně zadaný port pro smtp server, vypínám alert
                        self.port  = 0
                        self.state = False
                        
                elif key == "passw":
                    self.passw = val
                    
                elif key == "predmet":
                    self.predmet = val
                
                elif key == "sens1TempTop":
                    try:
                        self.sens1TempTop = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens1TempTop")
                        self.sens1TempTop = 0
                        
                elif key == "sens1TempBot":
                    try:
                        self.sens1TempBot = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens1TempBot")
                        self.sens1TempBot = 0
                        
                elif key == "sens1HumiTop":
                    try:
                        self.sens1HumiTop = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens1HumiTop")
                        self.sens1HumiTop = 0
                        
                elif key == "sens1HumiBot":
                    try:
                        self.sens1HumiBot = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens1HumiBot")
                        self.sens1HumiBot = 0
                        
                elif key == "sens2TempTop":
                    try:
                        self.sens2TempTop = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens2TempTop")
                        self.sens2TempTop = 0
                        
                elif key == "sens2TempBot":
                    try:
                        self.sens2TempBot = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens2TempBot")
                        self.sens2TempBot = 0
                        
                elif key == "sens2HumiTop":
                    try:
                        self.sens2HumiTop = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens2HumiTop")
                        self.sens2HumiTop = 0
                        
                elif key == "sens2HumiBot":
                    try:
                        self.sens2HumiBot = int(val)
                    except:
                        print ("vadný souboru alert.ini - sens2HumiBot")
                        self.sens2HumiBot = 0                    
                    
    def SaveSetup(self):
        '''uložení parametrů do souboru
        '''
        
        path = "config/alert.ini"
        
        with open(path,mode="w",encoding="utf-8") as f:
            
            f.write("# parametry pro odesílání alertů na email…\n")
            
            if self.state == True:
                f.write("state: On\n")
            else:
                f.write("state: Off\n")
                
            f.write("addrTo: {}\n".format(self.addrTo))
            f.write("smtp: {}\n".format(self.smtp))
            f.write("addrFrom: {}\n".format(self.addrFrom))
            f.write("port: {}\n".format(self.port))
            f.write("passw: {}\n".format(self.passw))
            f.write("predmet: {}\n".format(self.predmet))
            f.write(":sens1TempTop {}\n".format(self.sens1TempTop))
            f.write(":sens1TempBot {}\n".format(self.sens1TempBot))
            f.write(":sens1HumiTop {}\n".format(self.sens1HumiTop))
            f.write(":sens1HumiBot {}\n".format(self.sens1HumiBot))
            f.write(":sens2TempTop {}\n".format(self.sens2TempTop))
            f.write(":sens2TempBot {}\n".format(self.sens2TempBot))
            f.write(":sens2HumiTop {}\n".format(self.sens2HumiTop))
            f.write(":sens2HumiBot {}\n".format(self.sens2HumiBot))
    
    
            
if __name__ == "__main__":
    
    alert = Alert()

    alert.addrTo = "tomas.kuncl@gmail.com"
    
    alert.SaveSetup()
    
    