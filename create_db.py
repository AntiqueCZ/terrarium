#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sqlite3

path = "data/prusa.sqlite3"
db = sqlite3.connect(path)

cursor = db.cursor()

#########################################
# verze databáze
#########################################
# inkrementální číslo verze schématu databáze
cursor.execute('''
    PRAGMA user_version = 1
    ''')

db.commit()

#########################################
# prusa TABLE
#########################################
# v tabulce se ukládá timestamp v sec jako PRIMARY KEY
# teplota a vlhkost pro horní a spodní skříň
# time=časové razítko v sec od Epochy
cursor.execute('''
    CREATE TABLE IF NOT EXISTS prusa(
        timestamp TEXT PRIMARY KEY,
        top_temp TEXT,
        top_humi TEXT,
        bott_temp TEXT,
        bott_humi TEXT
    )
''')
db.commit()
db.close()