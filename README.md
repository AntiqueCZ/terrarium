
# Instalace projektu Terrarium na Rasspberry Pi Zero W

## 1) Příprava médií  

- stáhnout aktuální verzi Raspbian Pi OS Lite
  - [Raspbian Download Page](https://www.raspberrypi.org/downloads/raspbian/)
- karta µSD, min. 8GB, class 10, lépe HCI nebo XCI
- utilitou [Rufus](https://rufus.ie/) nahrát stažený obraz Raspbianu na µSD
   - na kartě se vytvoří oddíl boot (který je vidět ve Win10) a oddíl EXT4 (ten vidět není)
   - vše na kartě bude samozřejmě přepsáno


## 2) Nastavení WiFi a SSH přístupu

- vložit µSD do čtečky karet a připojit k Win10
- na rootu karty vytvořit prázdný soubor s názvem `ssh` (bez přípony)
- na rootu karty vytvořit soubor s názvem `wpa_supplicant.conf`
- do tohoto souboru vložit následující řádky:

```conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=CZ
network={
     ssid="název sítě WiFi"
     psk="heslo pro připojení k síti"
     key_mgmt=WPA-PSK
}
```
- v souboru přepsat název sítě a heslo na aktuální údaje


## 3) Instalace a základní nastavení Raspberry Pi OS

- po vložení µSD karty do RPi a zapnutí je nutno chvíli posečkat na první boot (provádí se ještě přeformátování/resize oddílů karty)
- k RPi ZeroW se lze připojit dvěma způsoby:
  - 1) klávesnice + monitor
    - klávesnice se musí připojit k µUSB konektoru přes OTG redukci
    - monitor/TV se připojuje kabelem miniHDMI/HDMI
  - 2) z PC přes protokol SSH
    - RPi zero W musí být připojené k WiFi
    - je nutno zjistit IP adresu přidělenou routerem (nebo zkusit zadat `raspberrypi.local`)
    - v programu [Putty](https://the.earth.li/~sgtatham/putty/latest/w64/putty.exe) zadat zjištěnou IP adresu, port 22 a stisknout Připojit
    - v otevřeném okně zadat username:pi a jako heslo raspberry (pozor, při zadávání hesla se nic nezobrazuje...)

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>  

### 3-1) Aktualizace seznamu balíčků a upgrade

```bash
# aktualizace - pozor, operace tak na 20-30 minut!
sudo apt update
sudo apt upgrade
```

### 3-2) Instalace základních programů potřebných pro běh ovládání terária

```bash
# Python v3.7 je již obsažen v Raspbian OS

# instalace PIP
sudo apt install python3-pip

# instalace GIT
sudo apt install git

```

### 3-3) Instalace doplňků pro Python

```bash
# instalace webového serveru Flask pro uživatele pi
python3 -m pip install Flask

# instalace knihovny pro přístup k NTP serverům
python3 -m pip install ntplib

# knihovny pro ovládání portů na RPi
sudo apt install python3-gpiozero

# ovladače adafruit pro DHT senzory
sudo apt install libgpiod2
python3 -m pip install adafruit-circuitpython-dht
```

### 3-4) Nastavení prostředí Raspi OS

- spustit systémový konfigurátor…

```bash
# spuštění konfigurace
sudo raspi-config
```

- …a pod položkou 1) změnit heslo pro uživatele `pi`
- znovu spusti konfigurátor a nastavit lokální prostředí (pol. 4 → I1 → mezerníkem zaškrtnout položku `cs_CZ.UTF8 UTF8` → jako defaultní zvolit `cs_CZ.UTF8`) 
- dále nastavit časovou zónu (pol. 4 → I2 → pol. Europe → pol. Prague)
- nastavit zemi pro WiFi (pol. 4 → I4 → zvolit CZ Czech Republic  )
- nastavit jméno zařízení, jak bude viditelné v síti LAN (pol. 2 → N1 → OK → zadat `terrarium`)
- zvolit Finish a potvrdit restart RPi Zero W

odteď se lze připojit k RPi zadáním adresy `terrarium.local`



## 4) Zkopírování výkonných kódů pro ovádání terária

- výkonné kódy jsou uloženy na servech GitLab, přístup pro stažení je bez autentizace

```bash
# zkopírování instalace TCU do RPi
cd /home/pi/
git clone "https://AntiqueCZ@gitlab.com/AntiqueCZ/terrarium"
```

- otestování funkčnosti:

```bash
# přesun do adresáře s kódy a skripty
cd /home/pi/terrarium

# spuštění programu
python3 main.py
```
- po několika vteřinách bude přístupný web na adrese: [http://terrarium.local:5000](http://terrarium.local:5000)


<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>  

## 5) Nastavení automatického spouštění kódů po zapnutí

- z dostupných možností funguje jen spouštění metodou skriptu `/etc/profiles`
- crontab spouští skripty jako root, bohužel pod root z nějakého důvodu nefunguje GPIO4 (pin7) jako pin pro AM2103
- systemd zase neumožňuje smysluplně spouštět aktualizační skript, při vypnutí služby si pod sebou kompletně podřízne větev

### 5-1) Nastavení Autologin pro uživatele pi

- provede se přes `sudo raspi-config`, položka 3: Boot Options → B1: Desktop/CLI → B2: Console Autologin

### 5-2) Nastavení spuštění skriptu

- v souboru `sudo nano /etc/profile` se na konec řádek přidají požadované příkazy, např.:

```bash
# test spuštěného procesu
# hledám ve spuštěných procesech název/cestu obsahující řetězec 'adafr'
# je to knihovna ovladače GPIO pro ovládání senzoru AM2303
if pgrep -f ".*adafr.*" > /dev/null
then
   echo "Program Terrarium již běží…"
else
   echo "Spouštím terrarium control"
   cd /home/pi/terrarium
   python3 main.py &
fi
```

- znak `&` znamená, že skript poběží na pozadí a systém zpřístupní příkazovou řádku

- nevýhodou je přístup skrz SSH, kdy se po každém přihlášení skript opět spustí

- na řetězec v podmínce proti opakovanému spouštění lze (díky parametru -f?) aplikovat regulární výrazy
