# -*- coding: utf-8 -*-

import pytest

from terrarium import Terra
from channel import OutChannel
from nastaveni import Interval

def test_Terra():
    
    # načtení a kontrola testovacích dat
    t = Terra()
    
    err = len(t.channel)
    assert err == 10
    
    err = len(t.interval)
    assert err>0
    
    err = t.channel[2].name
    assert err == "Sprcha Zelené ještěrky"
    
    err = t.interval[0].View("days")
    assert err == "po, út, pá<br />sv"
    
    err = t.interval[1].View("days")
    assert err == "st, čt<br />ne"    
    
    err = len(t.interval[0].ch_outs)
    assert err == 4
    
    err = len(t.interval[1].ch_outs)
    assert err == 3
    
    err = t.interval[0].temp
    assert err == None
    
    err = t.interval[0].humi
    assert err == None
    
    err = t.interval[1].View("temp")
    assert err == "25.6°C"
    
    err = t.interval[1].View("humi")
    assert err == "88.8%"
    