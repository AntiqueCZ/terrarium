class Interval:
    ''' třída pro data intervalů
    id      - ID intervalu (počítáno od nuly)
    od      - minuty (možno zadat v hh:mm)
    do      - minuty (možno zadat v hh:mm)
    days    - množina číselných označení dnů v týdnu
    ch_outs - množina id výstupních kanálů
    temp    - požadovaná teplota v 1/10° (275 => 27,5°C)  
    humi    - požadovaná vlhkost v 1/10% (459 => 45,9%)
    light   - bool (svítí | nesvítí)

    podle typu výstupního kanálu se ignorují hodnoty:
        - heat: .humi
        - humidity: .temp
        - light: .humi, .temp
    
    '''
    def  __init__(self):
        self.id = None
        self.od = None
        self.do = None
        self.days = set()
        self.ch_outs = set()
        self.ch_in = None
        self.temp = 0
        self.humi = 0
        self.light = None
        
        

    def SetOd(self,s_od):
        ''' nastavení parametru Od
        
        údaj ukládán v minutách od počátku dne (max tedy 24×60 = 1440 min)
        
        return 0 - OK
               1 - Error
        '''
        # očištění vstupu od bílého balastu
        buff = s_od.strip()

        # kontrola délky řetězce
        # očekávám formát hh:mm nebo h:mm
        if len(buff) != 5 and len(buff) != 4:
            print('Nesprávná délka vstupního řetězce')
            return 1

        try:
            if len(buff) == 5:
                hod = int(buff[:2])
            else:
                hod = int(buff[:1])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.od = None
            return 2
    
        try:
            min = int(buff[-2:])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.od = None
            return 3

        self.od = 60 * hod + min

        return 0

    def SetDo(self,s_do):
        ''' nastavení parametru Do
        
        údaj ukládán v minutách od počátku dne (max tedy 24×60 = 1440 min)
        
        return 0 - OK
               1 - Nesprávná délka řetězce 
               2 - nečíselné zadání hod
               3 - nečíselné zadání min
               4 - Do je menší nebo rovno Od
        '''
        # očištění vstupu od bílého balastu
        buff = s_do.strip()

        # kontrola délky řetězce
        # očekávám formát hh:mm
        if len(buff) != 5 and len(buff) != 4:
            print('Nesprávná délka vstupního řetězce')
            return 1

        try:
            if len(buff) == 5:
                hod = int(buff[:2])
            else:
                hod = int(buff[:1])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.do = None
            return 2
    
        try:
            min = int(buff[-2:])
            
        except ValueError:
            print("Chyba procesu: nečíselné zadání času")
            self.do = None
            return 3

        self.do = 60 * hod + min

        if self.od is not None:
            if self.do <= self.od:
                print("Chyba procesu: hodnota Do je menší, nebo rovna hodnotě Od")
                self.do = None
                return 4

        return 0
            
    def SetDays(self,l_days):
        ''' nastavení parametru Days (list of int) 
        
        s_days - získané pole (class 'list') z webového rozhraní
               
        days   - množina všech dní  = {1,2,3,4,5,6,7,8}
                 1 pondělí
                 2 úterý
                 3 středa
                 4 čtvrtek
                 5 pátek
                 6 sobota
                 7 neděle
                 8 svátky
        
        return 0 - OK
               1 - žádný den
        '''

        # vyprázdnění množiny
        self.days.clear()
        
        # kontrola prázdné množiny
        if len(l_days) == 0:
            return 1
        
        if l_days[0] == '':
            return 1

        # projdu postupně všechny položky pole
        # protože je to pole z checkboxů, není potřeba dalších kontrol validity dat
        for i in l_days:
            self.days.add(int(i))

        return 0

    def SetChannelOuts(self,l_chann):
        ''' nastavení parametru Cahnnel Outs (list of int) 
        
        s_days - získané pole (class 'list') z webového rozhraní
               
        return 0 - OK
               1 - žádný den
        '''

        # vyprázdnění množiny
        self.ch_outs.clear()
        
        # akceptuji i prázdnou množinu
        if len(l_chann) == 0:
            return 1

        # projdu postupně všechny položky pole
        # protože je to pole z checkboxů, není potřeba dalších kontrol validity dat
        for i in l_chann:
            self.ch_outs.add(int(i))

        return 0

    def View(self,v="",ch=dict()):
        ''' zobrazení hodnot pro html zobrazení
        '''
        
        if v=="od":
            # // je celočíselné dělení
            # %  je zbytek po dělění
            if self.od == None:
                return "None"

            return "{:0>2}:{:0>2}".format(self.od//60,self.od%60)
        
        elif v=="do":
            # // je celočíselné dělení
            # %  je zbytek po dělění
            if self.do == None:
                return "None"

            return "{:0>2}:{:0>2}".format(self.do//60,self.do%60)
        
        elif v=="days":
            buff = ""
            buff2= "" 
            
            print("debug")
            
            if 1 in self.days:
                buff = "po"
            
            if 2 in self.days:
                if len(buff)>0:
                    buff += ", út"
                else:
                    buff = "út"
            if 3 in self.days:
                if len(buff)>0:
                    buff += ", st"
                else:
                    buff = "st"
            if 4 in self.days:
                if len(buff)>0:
                    buff += ", čt"
                else:
                    buff = "čt"
            if 5 in self.days:
                if len(buff)>0:
                    buff += ", pá"
                else:
                    buff = "pá"  
            if 6 in self.days:
                if len(buff2)>0:
                    buff2 += ", so"
                else:
                    buff2 = "so"
            if 7 in self.days:
                if len(buff2)>0:
                    buff2 += ", ne"
                else:
                    buff2 = "ne"
            if 8 in self.days:
                if len(buff2)>0:
                    buff2 += ", sv"
                else:
                    buff2 = "sv"  
            
            if len(buff) == 0:
                return buff2
            
            else:
                return buff + "<br />" + buff2
            
        elif v=="temp":
            return "{:.1f}°C".format(self.temp/10)
        
        elif v=="humi":
            return "{:.1f}%".format(self.humi/10)
        
        elif v=="chann":
            buff=""
            
            for i in self.ch_outs:
                buff += ch[i].name + "<br />\n"
                
            return buff
        
        elif v=="light":
            if self.light == True:
                return "on"
            elif self.light == False:
                return "off"
            else:
                return "None"
        
        else:
            return "err"
        
    def Edit(self,v=""):
        ''' zobrazení hodnot pro html editaci
        '''
        if v=="od":
            return "{:0>2}:{:0>2}".format(self.od//60,self.od%60)

        elif v=="do":
            return "{:0>2}:{:0>2}".format(self.do//60,self.do%60)
        
        elif v=="temp":
            return "{:.1f}".format(self.temp/10)
        
        elif v=="humi":
            return "{:.1f}".format(self.humi/10)
        
    def SetTemp(self,s_temp):
        ''' převod řetězce na teplotu intervalu (1 | 65)
        
        return:
            0 - OK
            1 - prázdný řetězec, nastavuji 25°C
            2 - teplota mimo rozsah, beze změny
            3 - nečíselné zadání (ValueError), opět beze změny
        '''
        
        buff = s_temp.strip()
        
        # prázdný řetězec - nastavuji 25°C
        if len(buff) == 0:
            self.temp = 250
            return 1

        try:
            # náhrada čárky za tečku -> převod na float a vynásobení 10 -> převod na int
            x = int(float(buff.replace(',','.'))*10)

            if 10 < x < 650:
                self.temp = x
                return 0
            else:
                print("Chyba: požadovaná teplota je mimo povolený rozsah")
                return 2
            
        except ValueError:
            print("Chyba: nečíselné zadání teploty")
            return 3
        
    def SetHumi(self,s_humi):
        ''' převod řetězce na požadovanou vlhkost intervalu (1 | 99)
        
        return:
            0 - OK
            1 - prázdný řetězec, nastavuji 65%
            2 - vlhkost mimo rozsah, beze změny
            3 - nečíselné zadání (ValueError), opět beze změny
        '''
        
        buff = s_humi.strip()
        
        # prázdný řetězec - nastavuji 18°C
        if len(buff) == 0:
            self.humi = 650
            return 1

        try:
            # náhrada čárky za tečku -> převod na float a vynásobení 10 -> převod na int
            x = int(float(buff.replace(',','.'))*10)

            if 10 < x < 990:
                self.humi = x
                return 0
            else:
                print("Chyba: požadovaná teplota je mimo povolený rozsah")
                return 2
            
        except ValueError:
            print("Chyba: nečíselné zadání teploty")
            return 3
         
    def SetLight(self,s_light):
        ''' převod řetězce na požadovanstav osvětlení (on | off)
        
        return:
            0 - OK
            1 - chybné zadání, nastavuji None
            2 - nějaká chyba, nastavuji None
        '''
        
        buff = s_light.strip()
        
        try:
            if buff == "on":
                self.light = True
                return 0
            elif buff == "off":
                self.light = False
                return 0
            else:
                self.light = None
                return 1
        except:
            self.light = None
            return 2
        
                
    