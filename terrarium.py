# -*- coding: utf-8 -*-
from channel import OutChannel
from nastaveni import Interval


class Terra:
    ''' třída pro ovládání terrária
    
        
    '''
    
    # verze vydání
    VERSION = '1.9'

    # seznam výstupních kanálů
    channel=[]
    
    # seznam intervalů
    interval = []
    
    # hystereze je nastavena na 0,5°C (v 1/10°C) a 2% (v 1/10%)
    HYST_TEMP = 5
    HYST_HUMI = 20

    
    def  __init__(self):
        
        # inicializace všech kanálů triaků a relé
        with open("config/terrarium.csv", "r", encoding="utf-8") as f:
            
            # musí se načíst všechny výstupní kanály, 1-8 triaky a 9-10 relé
            i=0
            for line in f.readlines():
                
                # přeskočení prvního řádku
                if i==0:
                    i=i+1
                    continue
                
                # v každém řádku je uloženo id, typ výstupního kanálu a jeho jméno
                inp_id, inp_type, inp_name, inp_used, inp_sens, inp_pulse, inp_delay = line.strip().split(";")
                
                # kontrola konzistence dat
                if i!=int(inp_id)+1:
                    print("nekonzistentní data v konfiguračním souboru")
                    pass
                
                if inp_used == 'True':
                    inpB_used = True
                else:
                    inpB_used = False
                    
                if inp_sens == '0':
                    inpInt_sens = 0
                elif inp_sens == '1':
                    inpInt_sens = 1
                else:
                    inpInt_sens = None
                    
                try:
                    p = int(inp_pulse)
                    d = int(inp_delay)
                except:
                    p=0
                    d=0
    
                # vytvoření instance terminálu a její přidání do seznamu
                out = OutChannel(
                    id=int(inp_id),
                    type=inp_type,
                    name=inp_name,
                    used=inpB_used,
                    sens=inpInt_sens, 
                    pulse=p, 
                    delay=d
                    )

                self.channel.append(out)
                
                i=i+1
                
    
        with open("config/intervaly.csv", "r", encoding="utf-8") as f:
            
            i=0
            for line in f.readlines():
                
                # přeskočení prvního řádku
                if i==0:
                    i=i+1
                    continue
                
                # v každém řádku je uloženo: id;od;do;days;ch_outs;ch_in;temp;humi;light
                inp_id, inp_od, inp_do, inp_days, inp_ch_outs, inp_temp, inp_humi, inp_light = line.strip().split(";")
                

                
                # vytvoření instance nastavení a její přidání do seznamu
                buff = Interval()
                
                buff.id = int(inp_id)
                buff.od = int(inp_od)
                buff.do = int(inp_do)
                buff.SetDays(inp_days.split(' '))
                buff.SetChannelOuts(inp_ch_outs.split(' '))
                if inp_temp == '':
                    buff.temp = None
                else:
                    buff.temp = int(inp_temp)
                    
                if inp_humi == '':
                    buff.humi = None
                else:
                    buff.humi = int(inp_humi)
                if inp_light == '':
                    buff.light = None
                elif inp_light == 'True':
                    buff.light = True
                else:
                    buff.light = False
                
                self.interval.append(buff)

    def UlozitKonfiguraci(self):
        ''' uložení aktuální konfigurace do souboru
        
        '''
        
        with open("config/terrarium.csv", "w", encoding="utf-8") as f:
            
            f.write('id;type;name;used;sens;pulse;delay\n')
            
            for r in self.channel:
                
                buff = "{};{};{};{};{};{};{}\n".format(r.id,r.type,r.name,r.used,r.sens,r.pulse,r.delay)
                
                f.write(buff)

    def UlozitIntervaly(self):
        ''' uložení seznamu intervalů do souboru
        
        '''
        
        print("Ukládací rutina intervalů")
        
        with open("config/intervaly.csv", "w", encoding="utf-8") as f:
                        
            # při uložení dojde k přeindexování ID intervalů
            index = 0
        
            f.write('id;od;do;days;ch_outs;temp;humi;light\n')
            
            for i in self.interval:
            
                    # množinu indexů výstupních kanálů musím převést na řetězec
                    # kvůli zpětnému převodu na seznam (class 'list') musí být mezi položkami vložena mezera
                    ch=""
                    for b in i.ch_outs:
                        ch += str(b) + ' '

                    # množinu dní musím převést na řetězec
                    # kvůli převodu na seznam (class 'list') musí být mezi položkami vložena mezera
                    d=""
                    for b in i.days:
                        d += str(b) + ' '

                    if i.temp == None:
                        t = ''
                    else:
                        t = str(i.temp)

                    if i.humi == None:
                        h = ''
                    else:
                        h = str(i.humi)

                    if i.light == None:
                        l = ''
                    else:
                        l = str(i.light)

                    # sestavení řádku pro zápis do souboru
                    buff = "{};{};{};{};{};{};{};{}\n".format(index,i.od,i.do,d.strip(),ch.strip(),t,h,l)

                    print(buff)

                    # zápis řádku do souboru
                    f.write(buff)

                    index += 1              
                
    def AddNewInt(self):
        ''' přidání prázdného intervalu do seznamu
        
        '''
        buff = Interval()
        
        # počítání od nuly, proto nepřičítám jedničku!!!
        x = len(self.interval)
        
        buff.id = x
        buff.od = 360
        buff.do = 480
        buff.SetDays('')
        buff.SetChannelOuts('')
        buff.temp = 200
        buff.humi = 500
        buff.light = False
        
        self.interval.append(buff)
        
        return x
